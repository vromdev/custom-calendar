import { html, css, LitElement } from 'lit';
import { ifDefined } from 'lit/directives/if-defined.js';
import { classMap } from 'lit/directives/class-map.js';
import { UtilsService } from './services/utils.service.js';

/**
 * custom-calendar component with event display
 *
 * @customElement custom-calendar
 * @litElement
 */

export class CustomCalendar extends LitElement {
  constructor() {
    super();
    this.locale = 'en-EN';
    this.events = {};
  }

  connectedCallback() {
    super.connectedCallback();
    this.month = new Date().getMonth();
    this.year = new Date().getFullYear();
    this.buildCalendar();
  }

  static get properties() {
    return {
      /**
       * Current year
       * @property
       * @type { Number }
       */
      year: {
        type: Number,
        state: true,
        attribute: false,
      },
      /**
       * Current month index
       * @property
       * @type { Number }
       */
      month: {
        type: Number,
        state: true,
        attribute: false,
      },
      /**
       * Object used to build the calendar. Three properties: monthName, daysOfMonth and startsOn
       * @property
       * @type { Object }
       */
      calendar: {
        type: Object,
        state: true,
        attribute: false,
      },
      /**
       * Object with days as property keys and list of events as values
       * @property
       * @type { Object }
       */
      events: {
        type: Object,
      },
      /**
       * Locale used for the calendar. It admits all locales from Intl.DateTimeFormat API
       * @property
       * @type { String }
       */
      locale: {
        type: String,
      },
    };
  }

  static get styles() {
    return css`
      *,
      *::after,
      *::before {
        box-sizing: border-box;
      }

      :host {
        --bg-color: var(--cal-bg-color, hsl(0, 0%, 100%));
        --text-color-days: var(--cal-text-color-days, hsl(0, 0%, 0%));
        --text-color-heading: var(--cal-text-color-heading, hsl(210, 29%, 24%));
        --data-hint-bg: var(--cal-data-hint-bg, hsl(210, 29%, 24%));
        --data-hint-text: var(--cal-data-hint-text, hsl(0, 0%, 100%));
        --eventday-bg: var(--cal-event, hsla(210, 29%, 24%, 0.1));
        --eventday-bg-hover: var(--cal-event-hover, hsla(210, 29%, 24%, 0.2));
      }

      .calendar {
        max-width: 400px;
        margin-left: auto;
        margin-right: auto;
        background-color: var(--bg-color);
      }

      .calendar__controls {
        display: flex;
        align-items: center;
        height: 2rem;
        color: var(--text-color-heading);
      }

      .calendar__controls h2 {
        font-size: 1.2rem;
        line-height: 2rem;
        margin: 0;
        padding: 0;
      }

      .calendar__controls button {
        background-color: transparent;
        color: var(--text-color-heading);
        font-weight: 700;
        cursor: pointer;
        padding: 1rem;
        border: none;
      }

      .calendar__list {
        text-align: center;
        list-style: none;
        display: grid;
        grid-template-columns: repeat(7, 1fr);
        margin: 2rem 0 0;
        padding: 0;
      }

      .calendar__list-item {
        align-items: center;
        font-size: 1.75ch;
        min-width: 2.5rem;
        min-height: 2.5rem;
        display: flex;
        justify-content: center;
        flex-direction: column;
        color: var(--text-color-days);
        position: relative;
        user-select: none;
      }

      .calendar__list-item--event {
        background-color: var(--eventday-bg);
      }

      .calendar__list-item--event:hover {
        transition: background-color 300ms ease;
        background-color: var(--eventday-bg-hover);
        cursor: pointer;
      }

      .calendar__month {
        text-align: center;
        padding: 1rem;
        color: var(--text-color-heading);
      }

      .calendar__weekdays {
        color: var(--text-color-heading);
        font-weight: 700;
        font-size: 0.9rem;
      }

      .calendar__list-item[data-hint]::before {
        content: attr(data-hint);
        pointer-events: none;
        white-space: pre;
        position: absolute;
        color: var(--data-hint-text);
        background: var(--data-hint-bg);
        padding: 0.5rem 1rem;
        border-radius: 0.3rem;
        opacity: 0;
        visibility: none;
        bottom: calc(0.5rem + 100%);
        right: 50%;
        font-size: 1rem;
        text-align: left;
        transform: translateX(50%);
      }

      .calendar__list-item[data-hint]::after {
        content: '';
        pointer-events: none;
        width: 0;
        height: 0;
        position: absolute;
        border-color: var(--data-hint-bg);
        border-width: 0;
        border-style: solid;
        opacity: 0;
        visibility: none;
        border-top-width: 0.5rem;
        border-right-width: 0.5rem;
        border-right-color: hsla(0, 0%, 0%, 0);
        border-left-width: 0.5rem;
        border-left-color: hsla(0, 0%, 0%, 0);
        bottom: 100%;
        right: 50%;
        transform: translateX(50%);
      }

      .calendar__list-item[data-hint]:hover::before,
      .calendar__list-item[data-hint]:hover::after {
        visibility: visible;
        opacity: 1;
        transition: opacity 300ms ease;
      }
    `;
  }

  updateToPreviousYear() {
    this.year -= 1;
    this.buildCalendar();
  }

  updateToNextYear() {
    this.year += 1;
    this.buildCalendar();
  }

  updateToPreviousMonth() {
    this.year = this.month === 0 ? this.year - 1 : this.year;
    this.month = (this.month - 1) % 12;
    this.buildCalendar();
  }

  updateToNextMonth() {
    this.year = this.month === 11 ? this.year + 1 : this.year;
    this.month = (this.month + 1) % 12;
    this.buildCalendar();
  }

  buildCalendar() {
    this.weekDays = UtilsService.getWeekDays({ locale: this.locale });
    const { year, month, locale } = this;
    this.calendar = {
      daysOfMonth: UtilsService.getTotalMonthDays({ month, year }),
      monthName: UtilsService.getMonthName({ locale, month, year }),
      startsOn: UtilsService.getFirstWeekday({ month, year }),
    };
  }

  render() {
    const { daysOfMonth, startsOn, monthName } = this.calendar;
    return html` <article class="calendar">
      <div class="calendar__controls">
        <button id="prev-year-control" @click=${this.updateToPreviousYear}>
          &lt;
        </button>
        <h2 class="calendar__year">${this.year}</h2>
        <button id="next-year-control" @click=${this.updateToNextYear}>
          &gt;
        </button>
      </div>
      <div id="month-controls" class="calendar__controls">
        <button id="prev-month-control" @click=${this.updateToPreviousMonth}>
          &lt;
        </button>
        <h2 class="calendar__month">${monthName}</h2>
        <button id="next-month-control" @click=${this.updateToNextMonth}>
          &gt;
        </button>
      </div>
      <ol class="calendar__list">
        ${this.weekDays.map(
          weekday =>
            html`<li class="calendar__weekdays calendar__list-item">
              ${weekday}
            </li>`
        )}
        ${[...Array(daysOfMonth).keys()].map((day, i) => {
          const dayString = new Date(
            this.year,
            this.month,
            day + 1
          ).toLocaleDateString();
          const firstDayStyle =
            i === 0
              ? `grid-column-start: ${startsOn !== 0 ? startsOn : 7}`
              : null;
          let events;
          if (this.events) {
            events =
              dayString in this.events
                ? this.events[dayString].map(e => `•  ${e}`).join('\n')
                : null;
          }
          const withEvents = { 'calendar__list-item--event': events };
          return html`<li
            class="calendar__list-item ${classMap(withEvents)}"
            style=${ifDefined(firstDayStyle)}
            data-hint=${ifDefined(events)}
          >
            ${day + 1}
          </li>`;
        })}
      </ol>
    </article>`;
  }
}

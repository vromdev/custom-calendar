export class UtilsService {
  static capitalize(string) {
    return string.toUpperCase().slice(0, 1) + string.toLowerCase().slice(1);
  }

  static getMonthName({ locale, year, month }) {
    const formatter = new Intl.DateTimeFormat(locale, { month: 'long' });
    return UtilsService.capitalize(formatter.format(new Date(year, month)));
  }

  static getWeekDays({ locale }) {
    const formatter = new Intl.DateTimeFormat(locale, {
      weekday: 'short',
    });
    return [...Array(7).keys()].map(dayIndex => {
      const weekday = formatter.format(new Date(2022, 4, dayIndex + 2)); // 2 of May 2022 is Monday
      return UtilsService.capitalize(weekday);
    });
  }

  static getTotalMonthDays({ month, year }) {
    const nextMonthIndex = (month + 1) % 12;
    return new Date(year, nextMonthIndex, 0).getDate();
  }

  static getFirstWeekday({ month, year }) {
    return new Date(year, month, 1).getDay();
  }
}

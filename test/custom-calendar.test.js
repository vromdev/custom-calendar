import { html } from 'lit';
import { fixture, expect } from '@open-wc/testing';
import { UtilsService } from '../src/services/utils.service.js';

import '../custom-calendar.js';

const events = {
  '20/5/2022': ['Star Wars Obi-Wan Kenobi', 'We rule this city'],
  '27/5/2022': ['Red', 'El sastre de la mafia'],
};

describe('CustomCalendar', () => {
  it('can specify the locale of the calendar', async () => {
    const el = await fixture(
      html`<custom-calendar locale="es-ES"></custom-calendar>`
    );
    const firstWeekday = el.shadowRoot.querySelector('.calendar__weekdays');

    expect(el.locale).to.equal('es-ES');
    expect(firstWeekday.textContent.trim()).to.equal('Lun');
  });

  it('has en-EN locale as default of the calendar', async () => {
    const el = await fixture(html`<custom-calendar></custom-calendar>`);
    const firstWeekday = el.shadowRoot.querySelector('.calendar__weekdays');

    expect(el.locale).to.equal('en-EN');
    expect(firstWeekday.textContent.trim()).to.equal('Mon');
  });

  it('can receive an object with the events', async () => {
    const el = await fixture(
      html`<custom-calendar .events=${events}></custom-calendar>`
    );
    expect(el.events).to.equal(events);
  });

  it('shows current year on first display', async () => {
    const el = await fixture(html`<custom-calendar></custom-calendar>`);
    const year = el.shadowRoot.querySelector('.calendar__year');

    expect(+year.textContent).to.equal(new Date().getFullYear());
  });

  it('shows current month on display', async () => {
    const el = await fixture(html`<custom-calendar></custom-calendar>`);
    const month = el.shadowRoot.querySelector('.calendar__month');
    const formatter = new Intl.DateTimeFormat('en-EN', { month: 'long' });
    const monthName = UtilsService.capitalize(formatter.format(new Date()));
    expect(month.textContent).to.equal(monthName);
  });

  it('can change year on button click', async () => {
    const el = await fixture(
      html`<custom-calendar title="attribute title"></custom-calendar>`
    );
    el.shadowRoot.getElementById('next-year-control').click();
    el.shadowRoot.getElementById('prev-year-control').click();
  });

  it('can change month on button click', async () => {
    const el = await fixture(
      html`<custom-calendar title="attribute title"></custom-calendar>`
    );
    el.shadowRoot.getElementById('next-month-control').click();
    el.shadowRoot.getElementById('prev-month-control').click();
  });

  it('passes the a11y audit', async () => {
    const el = await fixture(html`<custom-calendar></custom-calendar>`);
    await expect(el).shadowDom.to.be.accessible();
  });
});

# \<custom-calendar>

This webcomponent follows the [open-wc](https://github.com/open-wc/open-wc) recommendation.

## Installation

```bash
npm i @vrom-dev/custom-calendar
```

## Props

Property | Optional | Type | Description
--- | ---  | --- | ---
locale | Yes | String | Specifies the language for the calendar. It accepts all locales from Intl.DateTimeFormat API
events | Yes | Object | Object with dates as keys and an array of events for each key. See usage section for an example


## Styling
It is possible to style the component overriding the following custom properties:

```css
custom-calendar {
  --cal-bg-color; /* Calendar background color */
  --cal-text-color-days; /* Calendar days text color */
  --cal-text-color-heading; /* Calendar weedays, month and year text color */
  --cal-data-hint-bg; /* Data hint background color */
  --cal-data-hint-text; /* Data hint text color */
  --cal-event; /* Event day background color */
  --cal-event-hover; /* Event day background color on hover */
}
```
## Usage

```html
<script type="module">
  import { html, render } from 'lit';
  import '../custom-calendar.js';

  const events = {
    '20/5/2022': ['Star Wars Obi-Wan Kenobi', 'We rule this city'],
    '27/5/2022': ['Red', 'El sastre de la mafia'],
  };
  render(
    html`<custom-calendar locale="es-ES" .events=${events}></custom-calendar>`
  );
</script>
```

## Linting and formatting

To scan the project for linting and formatting errors, run

```bash
npm run lint
```

To automatically fix linting and formatting errors, run

```bash
npm run format
```

## Testing with Web Test Runner

To execute a single test run:

```bash
npm run test
```

To run the tests in interactive watch mode run:

```bash
npm run test:watch
```

## Demoing with Storybook

To run a local instance of Storybook for your component, run

```bash
npm run storybook
```

To build a production version of Storybook, run

```bash
npm run storybook:build
```

## Tooling configs

For most of the tools, the configuration is in the `package.json` to minimize the amount of files in your project.

If you customize the configuration a lot, you can consider moving them to individual files.

## Local Demo with `web-dev-server`

```bash
npm start
```

To run a local development server that serves the basic demo located in `demo/index.html`

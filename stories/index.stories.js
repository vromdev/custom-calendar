import { html } from 'lit';
import '../custom-calendar.js';

export default {
  title: 'CustomCalendar',
  component: 'custom-calendar',
  argTypes: {
    locale: { control: 'text' },
    events: { control: 'object' },
  },
};

function Template({ locale = 'en-EN', events = {} }) {
  return html`
    <custom-calendar locale="${locale}" .events=${events}></custom-calendar>
  `;
}

export const Regular = Template.bind({});

export const CustomLocale = Template.bind({});
CustomLocale.args = {
  locale: 'es-ES',
};

export const withEvents = Template.bind({});
withEvents.args = {
  events: {
    '20/5/2022': ['Star Wars Obi-Wan Kenobi', 'We rule this city'],
    '27/5/2022': ['Red', 'El sastre de la mafia'],
  },
};
